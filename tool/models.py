from __future__ import unicode_literals
from django.db import models
from mptt.models import MPTTModel

class Tags(MPTTModel):
    title = models.CharField(max_length=30)
    parent = models.ForeignKey("self",blank=True,null=True,related_name="children")

    def __str__(self):
        return self.title

# Create your models here.
class Assets(models.Model):
    # id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=30)
    description = models.TextField(null=True)
    publication_date = models.DateField(null=True)
    tags = models.ManyToManyField(Tags)
    asset_file = models.FileField(null=True)

    def __str__(self):
        return self.name