#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Auther: Godfrey Huang
# Email: huangqiu@cgenter.com
#
# Created by Godfrey Huang on 2015-12-15.
# Copyright (c) 2015 cgenter.com. All rights reserved.
import os
from PySide import QtGui,QtCore

# from tool.models import Assets
# from tool.models import Tags

# from TreeWidgetItem import TreeWidgetItem

class TypeTreeWidget(QtGui.QTreeWidget):
    filterAssetsSignal = QtCore.Signal(list)
    def __init__(self,parent=None,label='Folder',dir=None):
        QtGui.QTreeWidget.__init__(self,parent)
        self.setColumnCount(1)
        self.setHeaderHidden(True)
        self.setHeaderLabel(label)
        self.setFrameStyle(QtGui.QFrame.Box | QtGui.QFrame.Sunken)
        self.dir = dir
        self.projectWidget = None
        self.connections()

    def setProjectWidget(self,projectWidget,*args):
        self.projectWidget = projectWidget

    def getProjectWidget(self,*args):
        return self.projectWidget

    def addItem(self,item,*args):
        newItem = QtGui.QTreeWidgetItem(self.invisibleRootItem())
        font = QtGui.QFont()
        font.setPointSize(9)
        newItem.setFont(0,font)
        newItem.setText(0,item)
        # newItem.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsDragEnabled)
        newItem.setFlags(newItem.flags()|QtCore.Qt.ItemIsUserCheckable)
        newItem.setCheckState(0,QtCore.Qt.Unchecked)
        newItem.setIcon(0,self.style().standardIcon(QtGui.QStyle.SP_DirIcon))
        return newItem

    def itemSelectionChangedCmd(self):
        selectedItems = self.selectedItems()
        if selectedItems:
            selectedItem = selectedItems[0]
            if selectedItem.checkState(0):
                selectedItem.setCheckState(0,QtCore.Qt.Unchecked)
            else:
                selectedItem.setCheckState(0,QtCore.Qt.Checked)
        assetList = []
        if self.projectWidget:
            typeTreeWidgetCheckItemList = []
            for i in range(0,self.topLevelItemCount()):
                item = self.topLevelItem(i)
                if item.checkState(0):
                    typeTreeWidgetCheckItemList.append(item)
            projectTreeWidgetCheckItemList = []
            for i in range(0,self.projectWidget.topLevelItemCount()):
                item = self.projectWidget.topLevelItem(i)
                if item.checkState(0):
                    projectTreeWidgetCheckItemList.append(item)

            assetTypeList = []
            for typeItem in typeTreeWidgetCheckItemList:
                typeItemName = typeItem.text(0)
                for projectItem in projectTreeWidgetCheckItemList:
                    projectItemName = projectItem.text(0)
                    assetTypePath = self.dir+'/'+projectItemName+'/assets/'+typeItemName
                    if os.path.isdir(assetTypePath):
                        assetTypeList.append(assetTypePath)
            for assetTypePath in assetTypeList:
                assetNameDirList = os.listdir(assetTypePath)
                for assetNameDir in assetNameDirList:
                    assetPath = assetTypePath+'/'+assetNameDir
                    if os.path.isdir(assetPath):
                        tempAssetFiles = os.listdir(assetPath)
                        for tempFile in tempAssetFiles:
                            if tempFile.find('rig.mb')!=-1 or tempFile.find('rig.ma')!=-1 or (tempFile.find('_set_')!=-1 and tempFile.find('.png')==-1):
                                assetList.append(assetTypePath+'/'+assetNameDir+'/'+tempFile)

        self.filterAssetsSignal.emit(assetList)

    def connections(self):
        self.clicked.connect(self.itemSelectionChangedCmd)