#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Auther: Godfrey Huang
# Email: huangqiu@cgenter.com
#
# Created by Godfrey Huang on 2015-12-15.
# Copyright (c) 2015 cgenter.com. All rights reserved.
import os
import subprocess
from functools import partial
from PySide import QtGui,QtCore

from utils.ScreenShot import ScreenShot

class AssetFrame(QtGui.QFrame):
    deleteSelectedSignal = QtCore.Signal()
    updateSelectedSignal = QtCore.Signal(QtGui.QFrame)
    def __init__(self,topWidget=None,parent=None,id=None,name=None,description=None,publicationData=None,tag=None,assetFilePath=None,*args):
        QtGui.QFrame.__init__(self,parent)
        self.topWidget = topWidget
        self.setObjectName("thumbViewFrame")
        self.thumbWidth = 242
        self.thumbHigh = 138
        self.assetId = id
        self.assetName = name
        self.assetDescription = description
        self.assetPublicationData = publicationData
        self.assetTag = tag
        self.assetFilePath = assetFilePath.replace('\\','\\\\')
        self.assetThumbPath = (os.path.dirname(assetFilePath)+r'/'+os.path.basename(assetFilePath).split('.')[0])+'.png'
        self.selectState = False
        self.setMinimumSize(QtCore.QSize(266, 198))
        self.setMaximumSize(QtCore.QSize(266, 198))
        self.setFrameShape(QtGui.QFrame.Box)
        self.setFrameShadow(QtGui.QFrame.Sunken)
        # self.setPalette(QtGui.QPalette(QtGui.QColor(60,60,60,255)))
        self.setLineWidth(0)
        self.createContents()

    def createContents(self):
        self.verticalLayout = QtGui.QVBoxLayout(self)
        self.verticalLayout.setSpacing(5)

        self.thumbLabel = QtGui.QLabel(self)
        self.thumbLabel.setMinimumSize(QtCore.QSize(self.thumbWidth,self.thumbHigh))
        self.thumbLabel.setMaximumSize(QtCore.QSize(self.thumbWidth,self.thumbHigh))
        self.verticalLayout.addWidget(self.thumbLabel)

        self.createActions()

        self.titleLabel = QtGui.QLabel(self)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.titleLabel.setFont(font)
        self.titleLabel.setText(self.assetName)
        # self.titleLabel.setEnabled(True)
        self.titleLabel.setMinimumSize(QtCore.QSize(242, 32))
        self.titleLabel.setMaximumSize(QtCore.QSize(242, 32))
        self.titleLabel.setFrameStyle(QtGui.QFrame.Panel | QtGui.QFrame.Sunken)
        # self.titleLabel.setReadOnly(True)
        self.verticalLayout.addWidget(self.titleLabel)
        self.update()

    def update(self, *args, **kwargs):
        if os.path.exists(self.assetThumbPath):
            self.thumbLabel.setPixmap(QtGui.QPixmap(self.assetThumbPath))
            self.thumbLabel.setScaledContents(True)

    def openIn(self,assetFilePath,software,*args):
        typeDict = {'ma':'mayaAscii','mb':'mayaBinary'}
        buffer = self.assetName.split('.')
        if buffer:
            fileName = buffer[0]
            fileType = typeDict[buffer[1]]
            if software=='maya':
                cmdStr = 'import maya.cmds as cmds\n'
                cmdStr += 'cmds.file("%s",r=True,type="%s",ignoreVersion=True,gl=True,mergeNamespacesOnClash=False,namespace="%s",options="v=0")' % (assetFilePath,fileType,fileName)
                exec(cmdStr)
            elif software=='explorer':
                # assetFilePath = assetFilePath.replace('\\\\','\\')
                # print r''+assetFilePath
                subprocess.Popen('explorer /n, /select ' + assetFilePath)

    def createActions(self):
        self.thumbLabel.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
        # add right menu
        openInAction = QtGui.QAction("Open In",self.thumbLabel)

        openInMenu = QtGui.QMenu(self.thumbLabel)
        openInMayaAction = QtGui.QAction("Maya [Reference]",self.thumbLabel)
        openInMayaAction.triggered.connect(partial(self.openIn,self.assetFilePath,'maya'))
        openInMaxAction = QtGui.QAction("3ds Max",self.thumbLabel)
        openInMaxAction.triggered.connect(partial(self.openIn,self.assetFilePath,'3dsMax'))
        openInMotionBuilderAction = QtGui.QAction("Motion Builder",self.thumbLabel)
        openInMotionBuilderAction.triggered.connect(partial(self.openIn,self.assetFilePath,'motionBuilder'))
        openInMudboxAction = QtGui.QAction("Mudbox",self.thumbLabel)
        openInMudboxAction.triggered.connect(partial(self.openIn,self.assetFilePath,'mudbox'))
        openInZBrushAction = QtGui.QAction("ZBrush",self.thumbLabel)
        openInZBrushAction.triggered.connect(partial(self.openIn,self.assetFilePath,'zbrush'))
        openInPhotoshopAction = QtGui.QAction("Photoshop",self.thumbLabel)
        openInPhotoshopAction.triggered.connect(partial(self.openIn,self.assetFilePath,'photoshop'))
        openInExplorerAction = QtGui.QAction("Explorer",self.thumbLabel)
        openInExplorerAction.triggered.connect(partial(self.openIn,self.assetFilePath,'explorer'))
        openInMenu.addAction(openInMayaAction)
        openInMenu.addAction(openInMaxAction)
        openInMenu.addAction(openInMotionBuilderAction)
        openInMenu.addAction(openInMudboxAction)
        openInMenu.addAction(openInZBrushAction)
        openInMenu.addAction(openInPhotoshopAction)
        openInMenu.addAction(openInExplorerAction)

        openInAction.setMenu(openInMenu)
        self.thumbLabel.addAction(openInAction)

        screenShotsAction = QtGui.QAction("Screen Shots",self.thumbLabel)
        screenShotsAction.triggered.connect(partial(ScreenShot,self))
        self.thumbLabel.addAction(screenShotsAction)

        deleteSelectedAction = QtGui.QAction('Delete Selected',self.thumbLabel)
        deleteSelectedAction.triggered.connect(self.deleteSelected)
        self.thumbLabel.addAction(deleteSelectedAction)

        if not self.assetId:
            # screenShotsAction.setEnabled(False)
            deleteSelectedAction.setEnabled(False)

    def deleteSelected(self):
        self.deleteSelectedSignal.emit()

    def setActive(self,state,*args):
        if state:
            self.selectState = True
            self.setStyleSheet('#thumbViewFrame {border: 3px solid #18a7e3;background-color: rgba(24, 167, 227, 25);}')
        else:
            self.selectState = False
            self.setStyleSheet("background-color: none")

    def mousePressEvent(self, event, *args, **kwargs):
        button = event.button()
        if button == QtCore.Qt.LeftButton:
            self.setActive(True)
            self.updateSelectedSignal.emit(self)
