#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Auther: Godfrey Huang
# Email: huangqiu@cgenter.com
#
# Created by Godfrey Huang on 2015-12-15.
# Copyright (c) 2015 cgenter.com. All rights reserved.
from PySide import QtGui

class TreeWidgetItem(QtGui.QTreeWidgetItem):
    def __init__(self,parent=None,tag=None,*args):
        QtGui.QTreeWidgetItem.__init__(self,parent)
        if tag:
            self.setTag(tag=tag)

    def setTag(self,tag,*args):
        self.tag = tag

    def getTag(self,*args):
        return self.tag