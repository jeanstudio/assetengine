#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Auther: Godfrey Huang
# Email: huangqiu@cgenter.com
#
# Created by Godfrey Huang on 2015-12-15.
# Copyright (c) 2015 cgenter.com. All rights reserved.
import os
from PySide import QtGui,QtCore

from tool.models import Assets
from tool.models import Tags

from AssetFrame import AssetFrame

class ScrollArea(QtGui.QScrollArea):
    fileDropped = QtCore.Signal(list)
    def __init__(self,parent=None,topWidget=None,*args):
        QtGui.QScrollArea.__init__(self,parent)
        self.topWidget = topWidget
        self.setObjectName('content_scrollArea')
        self.setAcceptDrops(True)
        self.setFrameShape(QtGui.QFrame.StyledPanel)
        self.setWidgetResizable(True)
        self.content()
        self.shiftPressed = False
        self.selecList = []

    def content(self):
        self.content_scrollAreaWidgetContents = QtGui.QWidget()
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.content_scrollAreaWidgetContents.sizePolicy().hasHeightForWidth())
        self.content_scrollAreaWidgetContents.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setWeight(50)
        font.setBold(False)
        self.content_scrollAreaWidgetContents.setFont(font)
        self.content_scrollAreaWidgetContents.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.content_scrollAreaWidgetContents.setInputMethodHints(QtCore.Qt.ImhNone)
        self.content_gridLayout = QtGui.QGridLayout(self.content_scrollAreaWidgetContents)
        self.setWidget(self.content_scrollAreaWidgetContents)

    def deleteSelected(self):
        for i in range(self.content_gridLayout.count()):
            widget = self.content_gridLayout.itemAt(i).widget()
            if widget.selectState:
                Assets.objects.filter(id=widget.assetId).delete()
                assetFilePath = widget.assetFilePath.replace('\\\\','\\')
                if os.path.isfile(assetFilePath):
                    os.remove(assetFilePath)
                assetThumbPath = (os.path.dirname(assetFilePath)+r'/'+os.path.basename(assetFilePath).split('.')[0])+'.png'
                if os.path.isfile(assetThumbPath):
                    os.remove(assetThumbPath)

        tags = Tags.objects.filter(title=self.currentSelectedTag)
        assets = Assets.objects.filter(tags=tags)
        self.updateContent(assets)

    def updateSelected(self,widget=None,*args):
        for i in range(self.content_gridLayout.count()):
            tempWidget = self.content_gridLayout.itemAt(i).widget()
            if widget is tempWidget:
                self.selecList.append(i)
            else:
                if not self.shiftPressed:
                    tempWidget.setActive(False)

        if self.shiftPressed and len(self.selecList)>1:
            if self.selecList[-2] < self.selecList[-1]:
                for j in range(self.selecList[-2],self.selecList[-1]+1):
                    self.content_gridLayout.itemAt(j).widget().setActive(True)
            else:
                for j in range(self.selecList[-1]+1,self.selecList[-2]):
                    self.content_gridLayout.itemAt(j).widget().setActive(True)

    def updateContent(self,assets=None,*args):
        # clear all frame
        while self.content_gridLayout.count()>0:
            widget = self.content_gridLayout.itemAt(0).widget()
            self.content_gridLayout.removeWidget(widget)
            widget.deleteLater()

        if assets:
            for i in range(len(assets)):
                asset = assets[i]
                assetType = type(asset)
                if assetType is Assets:
                    assetId = asset.id
                    assetName = asset.name
                    assetDescription = asset.description
                    assetPublicationDate = asset.publication_date
                    assetTag = asset.tags.all()[0].title
                    assetFilePath = asset.asset_file.path

                    thumbViewFrame = AssetFrame(
                        topWidget=self.topWidget,
                        parent=self.content_scrollAreaWidgetContents,
                        id=assetId,
                        name=assetName,
                        description=assetDescription,
                        publicationData=assetPublicationDate,
                        tag=assetTag,
                        assetFilePath=assetFilePath,
                    )
                    thumbViewFrame.deleteSelectedSignal.connect(self.deleteSelected)
                    thumbViewFrame.updateSelectedSignal.connect(self.updateSelected)
                    self.content_gridLayout.addWidget(thumbViewFrame, int(i/5), int(i%5), 1, 1)
                    self.currentSelectedTag = assetTag
                elif assetType is unicode:
                    assetName = os.path.basename(asset)
                    assetFilePath = asset
                    thumbViewFrame = AssetFrame(
                        topWidget=self.topWidget,
                        parent=self.content_scrollAreaWidgetContents,
                        # id=assetId,
                        name=assetName,
                        # description=assetDescription,
                        # publicationData=assetPublicationDate,
                        # tag=assetTag,
                        assetFilePath=assetFilePath,
                    )
                    thumbViewFrame.updateSelectedSignal.connect(self.updateSelected)
                    self.content_gridLayout.addWidget(thumbViewFrame, int(i/5), int(i%5), 1, 1)

    def restoreWidget(self):
        self.setStyleSheet("#content_scrollArea {background-color: none;}")

    def dragEnterEvent(self, event, *args, **kwargs):
        self.setStyleSheet('#content_scrollArea {border: 3px solid #ffffff;border-style: dashed;}')
        if event.mimeData().hasUrls:
            event.accept()
        else:
            event.ignore()
        # return QtGui.QScrollArea.dragEnterEvent(event)

    def dragLeaveEvent(self, event, *args, **kwargs):
        self.restoreWidget()
        # return QtGui.QScrollArea.dragLeaveEvent(event)

    def dragMoveEvent(self, event, *args, **kwargs):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()
        # return QtGui.QScrollArea.dragMoveEvent(event)

    def dropEvent(self, event, *args, **kwargs):
        # self.restoreWidget()
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            links = []
            for url in event.mimeData().urls():
                links.append(str(url.toLocalFile()))
            self.fileDropped.emit(links)
        else:
            event.ignore()
        # return QtGui.QScrollArea.dropEvent(event)

    def keyPressEvent(self, event):
        key = event.key()
        if key == QtCore.Qt.Key_Shift:
            self.shiftPressed = True
        return QtGui.QScrollArea.keyPressEvent(self,event)

    def keyReleaseEvent(self, event):
        key = event.key()
        if key == QtCore.Qt.Key_Shift:
            self.shiftPressed = False
        return QtGui.QScrollArea.keyReleaseEvent(self,event)

    def mousePressEvent(self, event, *args, **kwargs):
        button = event.button()
        if button == QtCore.Qt.LeftButton:
            self.updateSelected()
            self.selecList = []
