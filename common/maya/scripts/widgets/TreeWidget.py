#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Auther: Godfrey Huang
# Email: huangqiu@cgenter.com
#
# Created by Godfrey Huang on 2015-12-15.
# Copyright (c) 2015 cgenter.com. All rights reserved.
from PySide import QtGui,QtCore

from tool.models import Assets
from tool.models import Tags

from TreeWidgetItem import TreeWidgetItem

class TreeWidget(QtGui.QTreeWidget):
    filterAssetsSignal = QtCore.Signal(list)
    def __init__(self,parent=None,label='Folder'):
        QtGui.QTreeWidget.__init__(self,parent)
        self.setColumnCount(1)
        self.setHeaderHidden(True)
        self.setHeaderLabel(label)
        self.setFrameStyle(QtGui.QFrame.Box | QtGui.QFrame.Sunken)

        self.addRightKeyActions()
        self.connections()

    def addRightKeyActions(self, *args, **kwargs):
        self.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
        self.newTagAction = QtGui.QAction("New",self)
        self.newTagAction.triggered.connect(self.newTagActionCmd)
        self.addAction(self.newTagAction)

        self.editTagAction = QtGui.QAction("Edit",self)
        self.editTagAction.triggered.connect(self.editTagActionCmd)
        self.addAction(self.editTagAction)

        self.deleteTagAction = QtGui.QAction("Delete",self)
        self.deleteTagAction.triggered.connect(self.deleteTagActionCmd)
        self.deleteTagAction.setEnabled(False)
        self.addAction(self.deleteTagAction)

        self.cancelSelectAction = QtGui.QAction("Cancel Select",self)
        self.cancelSelectAction.triggered.connect(self.deselectActionCmd)
        self.addAction(self.cancelSelectAction)

    def getCurrentSelectedItem(self):
        selectedItems = self.selectedItems()
        if selectedItems:
            return selectedItems[0]
        else:
            return None

    def showDialog(self,title='',textInfo='',*args):
        text,ok = QtGui.QInputDialog.getText(self,title,textInfo)
        if ok:
            return str(text)
        else:
            return None

    def newTagActionCmd(self):
        newTagTitle = self.showDialog(title='Input Dialog',textInfo='Enter new tag name:')
        if newTagTitle:
            selectedItem = self.getCurrentSelectedItem()
            if selectedItem and selectedItem.text(0)!='uncatelogued':
                selectedTag = selectedItem.tag
                newTag = Tags(title=newTagTitle,parent=selectedTag)
                newTag.save()
                self.addItem(tag=newTag,item=selectedItem)
            else:
                newTag = Tags(title=newTagTitle,parent=None)
                newTag.save()
                self.addItem(tag=newTag,item=self.invisibleRootItem())

    def editTagActionCmd(self):
        newTagTitle = self.showDialog(title='Input Dialog',textInfo='Enter new tag name:')
        if newTagTitle:
            selectedItem = self.getCurrentSelectedItem()
            if selectedItem and selectedItem.text(0)!='uncatelogued':
                selectedTag = selectedItem.tag
                selectedTag.title = newTagTitle
                selectedTag.save()
                selectedItem.setText(0,newTagTitle)

    def deleteTagActionCmd(self):
        selectItem = self.getCurrentSelectedItem()
        if selectItem:
            parentItem = selectItem.parent()
            if parentItem:
                parentItem.removeChild(selectItem)
            else:
                self.invisibleRootItem().removeChild(selectItem)
            selectItem.tag.delete()

    def deselectActionCmd(self,*args):
        self.deleteTagAction.setEnabled(False)
        self.setCurrentItem(self.invisibleRootItem())

    def addItem(self,tag,item,*args):
        self.expandItem(item)
        newItem = TreeWidgetItem(parent=item,tag=tag)
        font = QtGui.QFont()
        font.setPointSize(9)
        newItem.setFont(0,font)
        newItem.setText(0,str(tag))
        item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsDragEnabled | QtCore.Qt.ItemIsEditable)
        item.setIcon(0,self.style().standardIcon(QtGui.QStyle.SP_DirIcon))
        return newItem

    def addItemIter(self,rootTag,treeWidgetItem,*args):
        childrens = rootTag.get_children()
        if childrens:
            for children in childrens:
                item = self.addItem(children,treeWidgetItem)
                if children.get_children():
                    self.addItemIter(rootTag=children,treeWidgetItem=item)

    def updateContent(self):
        self.clear()
        rootTags = Tags.objects.filter(parent=None)
        if rootTags:
            for rootTag in rootTags:
                rootItem = self.addItem(rootTag,self.invisibleRootItem())
                self.addItemIter(rootTag=rootTag,treeWidgetItem=rootItem)

    def itemSelectionChangedCmd(self):
        selectItem = self.getCurrentSelectedItem()
        if selectItem:
            if selectItem.text(0) == 'uncatelogued':
                self.deleteTagAction.setEnabled(False)
                self.editTagAction.setEnabled(False)
            else:
                self.deleteTagAction.setEnabled(True)
                self.editTagAction.setEnabled(True)
            selectTag = selectItem.tag
            assets = Assets.objects.filter(tags=selectTag)
            self.filterAssetsSignal.emit(assets)

    def connections(self):
        self.itemSelectionChanged.connect(self.itemSelectionChangedCmd)
