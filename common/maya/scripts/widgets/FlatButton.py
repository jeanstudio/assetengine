#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Auther: Godfrey Huang
# Email: huangqiu@cgenter.com
#
# Created by Godfrey Huang on 2015-12-15.
# Copyright (c) 2015 cgenter.com. All rights reserved.
import os
import subprocess
from functools import partial
from PySide import QtGui,QtCore

class FlatButton(QtGui.QPushButton):
    def __init__(self,parent=None,*args):
        QtGui.QPushButton.__init__(self,parent)
        self.font = QtGui.QFont()
        self.font.setPointSize(9)
        self.setFont(self.font)
        self.setFlat(True)

    def enterEvent(self, *args, **kwargs):
        self.setStyleSheet('color:rgb(255,0,0);')

    def leaveEvent(self, *args, **kwargs):
        self.setStyleSheet('color:none')