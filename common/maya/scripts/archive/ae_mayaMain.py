#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Auther: Godfrey Huang
# Email: huangqiu@cgenter.com
#
# Created by Godfrey Huang on 2015-8-01.
# Copyright (c) 2014 cgenter.com. All rights reserved.

import os,sys
os.environ['DJANGO_SETTINGS_MODULE'] = 'assetEngine.settings'
import django
django.setup()

from PySide import QtGui
import shiboken
from common.maya.scripts.ui.MainWindow import MainWindow

import maya.OpenMayaUI as omui
import pymel.core as pm

def getMayaWindow():
    ptr = omui.MQtUtil.mainWindow()
    return shiboken.wrapInstance(long(ptr),QtGui.QWidget)

def main():
    if pm.window(ae_mainWindow.win._windowName, exists=True):
        pm.deleteUI(ae_mainWindow.win._windowName, window=True)
    win = MainWindow(parent=getMayaWindow())
    win.show()

if __name__=='__main__':
    main()