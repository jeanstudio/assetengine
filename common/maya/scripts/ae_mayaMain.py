#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Auther: Godfrey Huang
# Email: huangqiu@cgenter.com
#
# Created by Godfrey Huang on 2015-8-01.
# Copyright (c) 2014 cgenter.com. All rights reserved.

import os,sys
os.environ['DJANGO_SETTINGS_MODULE'] = 'assetEngine.settings'
import django
django.setup()

from PySide import QtGui,QtCore
import shiboken
# from window.MainWindow import MainWindow
from window.LoginDialog import LoginDialog

import maya.OpenMayaUI as omui
import pymel.core as pm

def getMayaWindow():
    ptr = omui.MQtUtil.mainWindow()
    return shiboken.wrapInstance(long(ptr),QtGui.QWidget)

def main():
    # if pm.window(MainWindow._windowName, exists=True):
    #     pm.deleteUI(MainWindow._windowName, window=True)
    # win = MainWindow(parent=getMayaWindow())
    # win.show()
    # if pm.window(LoginDialog._windowName, exists=True):
    #     pm.deleteUI(LoginDialog._windowName, window=True)
    global login
    try:
        login.close()
        login.delteLater()
    except:pass
    login = LoginDialog(parent=getMayaWindow())
    login.show()

if __name__=='__main__':
    main()