#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Auther: Godfrey Huang
# Email: huangqiu@cgenter.com
#
# Created by Godfrey Huang on 2015-12-15.
# Copyright (c) 2015 cgenter.com. All rights reserved.
import os,sys,time
os.environ['DJANGO_SETTINGS_MODULE'] = 'assetEngine.settings'
import django
django.setup()

from PySide import QtGui

from window.MainWindow import MainWindow
from window.LoginDialog import LoginDialog

def main():
    app = QtGui.QApplication(sys.argv)
    # win = MainWindow()
    # win.show()
    login = LoginDialog()
    login.show()
    app.exec_()

if __name__=='__main__':
    main()