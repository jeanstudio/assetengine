#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Auther: Godfrey Huang
# Email: huangqiu@cgenter.com
#
# Created by Godfrey Huang on 2015-12-15.
# Copyright (c) 2015 cgenter.com. All rights reserved.

import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'assetEngine.settings'
import django
django.setup()