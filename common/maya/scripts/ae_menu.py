#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Auther: Godfrey Huang
# Email: huangqiu@cgenter.com
#
# Created by Godfrey Huang on 2015-12-15.
# Copyright (c) 2015 cgenter.com. All rights reserved.
import pymel.core as pm

class mainMenu():
	def __init__(self,*args):
		self.name = 'assetEngineMenu'

		if pm.menu(self.name,exists=True):
			pm.deleteUI(self.name)
		self.create()

	def create(self,*args):
		mainMenu = pm.menu(self.name,label="Asset Engine",aob=True,tearOff=True,parent="MayaWindow")
		# --------------------refresh menu-------------------------
		pm.menuItem(label='Main',annotation='Main',command='import ae_mayaMain as ae_mm\nreload(ae_mm)\nae_mm.main()',parent=mainMenu)
		pm.menuItem(label='Refresh',annotation='Refresh',image='Refresh-50.png',command=self.refreshMenu,parent=mainMenu)

	def refreshMenu(self,*args):
		import ae_menu as am
		reload(am)
		am.main()

def main(*args):
	mainMenu()

if __name__ == '__main__':
	main()
