#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Auther: Godfrey Huang
# Email: huangqiu@cgenter.com
#
# Created by Godfrey Huang on 2015-12-15.
# Copyright (c) 2015 cgenter.com. All rights reserved.
import os,gettext

def translator(langFileName,language,*args):
    if langFileName and language:
        BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
        localePath = BASE_DIR+'/locale'
        gettext.install(langFileName, localePath, unicode=True)
        gettext.translation(langFileName, localePath, languages=[language]).install(True)