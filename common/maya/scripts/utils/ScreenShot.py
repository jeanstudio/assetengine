#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Auther: Godfrey Huang
# Email: huangqiu@cgenter.com
#
# Created by Godfrey Huang on 2015-12-15.
# Copyright (c) 2015 cgenter.com. All rights reserved.
from PySide import QtGui,QtCore

class ScreenShot_old(QtGui.QLabel):
    # doneSignal = pyqtSignal(QtGui.QPixmap)
    def __init__(self, parent=None,frame=None,picPath=None,*args):
        # super(ScreenShot, self).__init__(parent)
        QtGui.QLabel.__init__(self,parent)
        self.frame = frame
        self.picPath = picPath
        self.pixmap = self.takeScreenshot()
        self.setPixmap(self.pixmap)
        self.setCursor(QtCore.Qt.CrossCursor)
        # self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint | QtCore.Qt.FramelessWindowHint)
        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint | QtCore.Qt.FramelessWindowHint | QtCore.Qt.Tool)
        self.showFullScreen()
        self.move(0, 0)
        self.resize(self.pixmap.size())
        self.selected = QtCore.QRect(0, 0, 0, 0)
        self.first_point = None

    def getMultiScreenSize(self):
        desktop = QtGui.QApplication.desktop()
        width, height = 0, 0
        for index in range(desktop.screenCount()):
            bottom_right = desktop.screenGeometry(index).bottomRight()
            x, y = bottom_right.x(), bottom_right.y()
            if x > width:
                width = x
            if y > height:
                height = y
        return width, height

    def takeScreenshot(self):
        width, height = self.getMultiScreenSize()
        pixmap = QtGui.QPixmap.grabWindow(QtGui.QApplication.desktop().winId(),0, 0,width, height)
        return pixmap

    def paintEvent(self, event):
        # super(ScreenShot, self).paintEvent(event)
        painter = QtGui.QPainter(self)
        drawRegion = QtGui.QRegion(self.pixmap.rect())
        drawRegion = drawRegion.subtracted(QtGui.QRegion(self.selected))
        painter.setClipRegion(drawRegion)
        painter.setBrush(QtGui.QBrush(QtGui.QColor(0, 0, 0, 120)))
        painter.drawRect(self.pixmap.rect())
        painter.setClipping(False)
        if self.selected:
            painter.setPen(QtGui.QPen(QtGui.QColor(255, 255, 255)))
            painter.setBrush(QtGui.QBrush(QtGui.QColor(0, 0, 0, 0)))
            vertical = self.selected.normalized()
            vertical.setLeft(-1)
            vertical.setRight(self.pixmap.rect().right() + 1)
            horizontal = self.selected.normalized()
            horizontal.setTop(-1)
            horizontal.setBottom(self.pixmap.rect().bottom() + 1)
            painter.drawRects((vertical,horizontal))
            bound = self.pixmap.rect()
            bound.setBottomRight(self.selected.topLeft() - QtCore.QPoint(5, 5))
            painter.drawText(
                bound,
                QtCore.Qt.AlignBottom | QtCore.Qt.AlignRight,
                '(%d, %d)' % (self.selected.topLeft().x(), self.selected.topLeft().y())
            )
            bound = self.pixmap.rect()
            bound.setTopLeft(self.selected.bottomRight() + QtCore.QPoint(5, 5))
            painter.drawText(
                bound,
                QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft,
                '(%d, %d)' % (self.selected.width(), self.selected.height())
            )

    def keyPressEvent(self, event):
        key = event.key()
        if key == QtCore.Qt.Key_Escape:
            self.close()

    def mousePressEvent(self, event):
        self.first_point = event.pos()
        self.selected.setTopLeft(self.first_point)

    def mouseMoveEvent(self, event):
        self.move_selection(event.pos())

    def mouseReleaseEvent(self, event):
        self.move_selection(event.pos())
        self.finish()

    def move_selection(self, now_point):
        if self.first_point:
            self.selected = QtCore.QRect(self.first_point, now_point).normalized()
            self.update()

    def finish(self):
        tempixmap = self.pixmap.copy(self.selected)
        tempixmap.save(self.picPath,'PNG')
        self.close()
        self.frame.update()

class ScreenShot(QtGui.QLabel):
    def __init__(self,frame=None,*args):
        QtGui.QLabel.__init__(self,frame.topWidget)
        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint | QtCore.Qt.FramelessWindowHint | QtCore.Qt.Tool)
        self.pixmap = self.takeScreenshot()
        self.setPixmap(self.pixmap)
        self.setCursor(QtCore.Qt.CrossCursor)
        self.showFullScreen()
        self.move(0,0)
        self.resize(self.pixmap.size())
        self.x1 = -1
        self.y1 = -1
        self.startPoint = QtCore.QPoint()
        self.endPoint = QtCore.QPoint()
        self.picPath = frame.assetThumbPath
        self.frame = frame
        self.rubberBand = QtGui.QRubberBand(QtGui.QRubberBand.Rectangle,self)

    def takeScreenshot(self,*args):
        width, height = self.getMultiScreenSize()
        pixmap = QtGui.QPixmap.grabWindow(
            QtGui.QApplication.desktop().winId(),
            0, 0,
            width, height
        )
        return pixmap

    def getMultiScreenSize(self):
        desktop = QtGui.QApplication.desktop()
        width, height = 0, 0
        for index in range(desktop.screenCount()):
            bottom_right = desktop.screenGeometry(index).bottomRight()
            x, y = bottom_right.x(), bottom_right.y()
            if x > width:
                width = x
            if y > height:
                height = y
        return width, height

    def mousePressEvent(self,event):
        self.startPoint = event.pos()
        if not self.rubberBand:
            self.rubberBand = QtGui.QRubberBand(QtGui.QRubberBand.Rectangle,self)
        self.rubberBand.setGeometry(QtCore.QRect(self.startPoint,QtCore.QSize()))
        self.rubberBand.show()
        # self.update()

    def mouseMoveEvent(self,event):
        currentRealPos = event.pos()
        self.x1 = event.x()
        self.y1 = event.y()
        self.currentPos = QtCore.QPoint(currentRealPos.x(),((currentRealPos.x()-self.startPoint.x())*(138/242.0)+self.startPoint.y()))
        self.rubberBand.setGeometry(QtCore.QRect(self.startPoint,self.currentPos).normalized())
        self.update()

    def mouseReleaseEvent(self,event):
        # self.endPoint = event.pos()
        x = self.startPoint.x()+1
        y = self.startPoint.y()+1
        w = self.currentPos.x() - x
        h = self.currentPos.y() - y
        tempixmap = QtGui.QPixmap.grabWidget(self,x,y,w,h)
        tempixmap.save(self.picPath,'PNG')
        self.rubberBand.hide()
        self.hide()
        self.update()
        self.frame.update()
        # self.parentClass.updateGridLayout()
