#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Auther: Godfrey Huang
# Email: huangqiu@cgenter.com
#
# Created by Godfrey Huang on 2015-12-15.
# Copyright (c) 2015 cgenter.com. All rights reserved.

import sys
from PySide import QtGui,QtCore

class MessageBox(QtGui.QMessageBox):
    def __init__(self,parent=None,title='Message',text='',*args):
        QtGui.QMessageBox.__init__(self,parent)
        self.setWindowTitle(title)
        self.setText(text)