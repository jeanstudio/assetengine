#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Auther: Godfrey Huang
# Email: huangqiu@cgenter.com
#
# Created by Godfrey Huang on 2015-12-15.
# Copyright (c) 2015 cgenter.com. All rights reserved.

import sys,os
from PySide import QtGui,QtCore
from functools import partial
import gettext

import ae_login_ui as ae_lg_ui
from widgets.FlatButton import FlatButton
from RegisterDialog import RegisterDialog
from MainWindow import MainWindow
from Dialog import MessageBox

# from tool.models import Assets
# from tool.models import Tags
# from tool.models import User
# from tool.models import Group
from django.contrib.auth import authenticate
import utils.Translator as translator

class LoginDialog(QtGui.QDialog):
    _windowName = 'jn_loginDialog'
    def __init__(self,parent=None,*args):
        QtGui.QDialog.__init__(self,parent)
        self.setObjectName(self._windowName)
        langFileName = os.path.basename(os.path.abspath(__file__)).split('.')[0]
        translator.translator(langFileName=langFileName,language='zh_CN')
        self.parent = parent
        self.ui = ae_lg_ui.Ui_login_Dialog()
        self.ui.setupUi(self)
        self.content()
        self.setting()
        self.settings = None
        self.readSettings()
        self.connections()

    def content(self):
        self.ui.setting_horizontalLayout.addWidget(QtGui.QLabel(u'·'))
        self.ui.forgot_password_pushButton = FlatButton()
        self.ui.forgot_password_pushButton.setCursor(QtCore.Qt.OpenHandCursor)
        self.ui.forgot_password_pushButton.setText(_('Forgot password'))
        self.ui.setting_horizontalLayout.addWidget(self.ui.forgot_password_pushButton)

        self.ui.setting_horizontalLayout.addWidget(QtGui.QLabel(u'·'))
        self.ui.register_pushButton = FlatButton()
        self.ui.register_pushButton.setCursor(QtCore.Qt.OpenHandCursor)
        self.ui.register_pushButton.setText(_('Sign up now'))
        self.ui.setting_horizontalLayout.addWidget(self.ui.register_pushButton)

    def login(self):
        username = self.ui.username_lineEdit.text()
        password = self.ui.password_lineEdit.text()
        user = authenticate(username=username,password=password)
        if user is not None:
            if user.is_active:
                self.close()
                win = MainWindow()
                win.show()
        else:
            # self.ui.username_lineEdit.setText('Invalid username')
            self.ui.username_lineEdit.setStyleSheet('color: rgb(255, 0, 0);')

    def register(self):
        self.close()
        regDia = RegisterDialog(self)
        regDia.show()

    def setting(self):
        # window
        self.setWindowFlags(QtCore.Qt.Dialog | QtCore.Qt.FramelessWindowHint)

        # widget
        self.ui.username_lineEdit.setPlaceholderText(_('Username'))
        self.ui.password_lineEdit.setPlaceholderText(_('Password'))
        self.ui.password_lineEdit.setEchoMode(QtGui.QLineEdit.Password)
        self.ui.password_lineEdit.setInputMethodHints(QtCore.Qt.ImhHiddenText | QtCore.Qt.ImhNoPredictiveText | QtCore.Qt.ImhNoAutoUppercase)

    def usernameLineEditTextChangedCmd(self):
        self.ui.username_lineEdit.setStyleSheet('border-color:none')
        self.ui.password_lineEdit.clear()

    def connections(self):
        self.ui.login_pushButton.clicked.connect(self.login)
        self.ui.register_pushButton.clicked.connect(self.register)
        self.ui.username_lineEdit.textChanged.connect(self.usernameLineEditTextChangedCmd)
        self.ui.close_toolButton.clicked.connect(self.close)

    # update dialog position
    def mousePressEvent(self, event, *args, **kwargs):
        self.mouseClickXPos = event.x()
        self.mouseClickYPos = event.y()

    def mouseMoveEvent(self, event, *args, **kwargs):
        self.move(event.globalX()-self.mouseClickXPos,event.globalY()-self.mouseClickYPos)

    # event : QCloseEvent
    def closeEvent(self,event,*args):
        self.writeSettings()
        event.accept()

    def writeSettings(self):
        self.settings = QtCore.QSettings('CGENTER','AssetEngine')
        self.settings.beginGroup('LoginDialog')
        username = self.ui.username_lineEdit.text()
        self.settings.setValue('username',username)
        self.settings.setValue('password',self.ui.password_lineEdit.text())
        self.settings.setValue('remember',self.ui.remember_checkBox.checkState())
        self.settings.endGroup()

    def readSettings(self):
        self.settings = QtCore.QSettings('CGENTER','AssetEngine')
        self.settings.beginGroup('LoginDialog')
        username = self.settings.value('username')
        if username:
            self.ui.username_lineEdit.setText(username)
        rememberCheckState = self.settings.value('remember')
        if rememberCheckState == 2:
            self.ui.password_lineEdit.setText(self.settings.value('password'))
            self.ui.remember_checkBox.setCheckState(QtCore.Qt.Checked)
        self.settings.endGroup()
