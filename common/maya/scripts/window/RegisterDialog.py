#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Auther: Godfrey Huang
# Email: huangqiu@cgenter.com
#
# Created by Godfrey Huang on 2015-12-15.
# Copyright (c) 2015 cgenter.com. All rights reserved.
import os
from PySide import QtGui,QtCore
from functools import partial

import ae_register_ui as ae_rs_ui
from Dialog import MessageBox
# from tool.models import Assets
# from tool.models import Tags
# from tool.models import User
# from tool.models import Group
from django.contrib.auth.models import User
from django.contrib.auth.models import Group

from utils.Translator import translator

class RegisterDialog(QtGui.QDialog):
    _windowName = 'jn_registerDialog'
    def __init__(self,parent=None,*args):
        QtGui.QDialog.__init__(self,parent)
        self.setObjectName(self._windowName)
        langFileName = os.path.basename(os.path.abspath(__file__)).split('.')[0]
        translator(langFileName=langFileName,language='zh_CN')
        self.parent = parent
        self.ui = ae_rs_ui.Ui_register_Dialog()
        self.ui.setupUi(self)
        self.setting()
        self.connections()

    def addUser(self,username=None,email=None,password=None,*args):
        artistGroupList = Group.objects.filter(name='artist')
        if artistGroupList:
            if User.objects.filter(username=username):
                self.ui.username_lineEdit.setStyleSheet('color: rgb(255, 0, 0);')
                return False
            user = User.objects.create_user(username=username,email=email,password=password)
            if user.is_active:
                artistGroup = artistGroupList[0]
                user.groups.add(artistGroup)
                return True
            else:
                return False
        else:
            return False

    def register(self):
        username = self.ui.username_lineEdit.text()
        password = self.ui.password_lineEdit.text()
        email = self.ui.email_lineEdit.text()

        state = self.addUser(username=username,email=email,password=password)
        if state:
            infoDig = MessageBox(
                parent=self,
                title='Congrations:',
                text='Your account register succeed!\n\nClick "OK" to login dialog.'
            )
            if infoDig.exec_()==QtGui.QMessageBox.Ok:
                self.switchToLoinDialog()

    def showDialog(self,title='',textInfo='',*args):
        text,ok = QtGui.QInputDialog.getText(self,title,textInfo)
        if ok:
            return str(text)
        else:
            return None

    def switchToLoinDialog(self):
        self.close()
        self.parent.show()

    def setting(self):
        # window
        self.setWindowFlags(QtCore.Qt.Dialog | QtCore.Qt.FramelessWindowHint)

        # widgets
        self.ui.username_lineEdit.setPlaceholderText(_('Username'))
        self.ui.password_lineEdit.setPlaceholderText(_('Password'))
        self.ui.email_lineEdit.setPlaceholderText(_('Email'))
        self.ui.password_lineEdit.setEchoMode(QtGui.QLineEdit.Password)
        self.ui.password_lineEdit.setInputMethodHints(QtCore.Qt.ImhHiddenText | QtCore.Qt.ImhNoPredictiveText | QtCore.Qt.ImhNoAutoUppercase)

    def initWidgetsColor(self):
        self.ui.username_lineEdit.setStyleSheet('border-color:none')
        self.ui.password_lineEdit.setStyleSheet('border-color:none')
        self.ui.email_lineEdit.setStyleSheet('border-color:none')

    def connections(self):
        self.ui.register_pushButton.clicked.connect(self.register)
        self.ui.close_toolButton.clicked.connect(self.switchToLoinDialog)
        self.ui.username_lineEdit.textChanged.connect(self.initWidgetsColor)
        self.ui.password_lineEdit.textChanged.connect(self.initWidgetsColor)
        self.ui.email_lineEdit.textChanged.connect(self.initWidgetsColor)

    # update dialog position
    def mousePressEvent(self, event, *args, **kwargs):
        self.mouseClickXPos = event.x()
        self.mouseClickYPos = event.y()

    def mouseMoveEvent(self, event, *args, **kwargs):
        self.move(event.globalX()-self.mouseClickXPos,event.globalY()-self.mouseClickYPos)
