#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Auther: Godfrey Huang
# Email: huangqiu@cgenter.com
#
# Created by Godfrey Huang on 2015-12-15.
# Copyright (c) 2015 cgenter.com. All rights reserved.

from PySide import QtGui,QtCore
import ae_createAssets_ui as ae_ca_ui
reload(ae_ca_ui)

class CreateAssetDialog(QtGui.QDialog):
    _dialogName = 'jn_createAssetsWindow'
    fileDropped = QtCore.Signal(list)
    def __init__(self,parent=None,*args):
        QtGui.QDialog.__init__(self,parent)
        self.setWindowFlags(QtCore.Qt.Tool)
        self.ui = ae_ca_ui.Ui_Dialog()
        self.ui.setupUi(self)
        self.setObjectName(self._dialogName)

        self.ui.uploaded_file_frame.setAcceptDrops(True)

        self.connections()

    def dragEnterEvent(self, event, *args, **kwargs):
        if event.mimeData().hasUrls:
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event, *args, **kwargs):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event, *args, **kwargs):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            links = []
            for url in event.mimeData().urls():
                links.append(str(url.toLocalFile()))
            self.fileDropped.emit(links)
        else:
            event.ignore()

    def dragFile(self,fileLinks,*args):
        print fileLinks

    def connections(self):
        self.fileDropped.connect(self.dragFile)