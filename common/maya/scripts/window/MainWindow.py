#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Auther: Godfrey Huang
# Email: huangqiu@cgenter.com
#
# Created by Godfrey Huang on 2015-12-15.
# Copyright (c) 2015 cgenter.com. All rights reserved.
import datetime
# import hashlib
import os
# import shutil
from PySide import QtGui,QtCore
from functools import partial

from ae_assetEngine_ui import Ui_MainWindow
# from CreateAssetDialog import CreateAssetDialog

from tool.models import Assets
from tool.models import Tags
from django.core.files import File

from widgets.TreeWidget import TreeWidget
from widgets.ProjectTreeWidget import ProjectTreeWidget
from widgets.TypeTreeWidget import TypeTreeWidget
from widgets.ScrollArea import ScrollArea

class MainWindow(QtGui.QMainWindow):
    _windowName = 'jn_assetEngineWindow'
    def __init__(self,parent=None,*args):
        QtGui.QMainWindow.__init__(self,parent)
        self.projectDir = r'/home/godfrey/maya/projects'
        if parent:
            self.topWidget = parent
        else:
            self.topWidget = self
        self.initTags()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.setObjectName(self._windowName)
        self.createMenuBar()
        self.createAccountMenuBar()
        self.createSideBar()
        self.viewStyle = 'Thumb'
        if self.viewStyle == 'Thumb':
            self.createScrollArea()
        self.settings = None
        self.readSettings()
        self.connections()

    def initTags(self):
        if not Tags.objects.filter(title='uncatelogued'):
            uncateloguedTag = Tags(title='uncatelogued',parent=None)
            uncateloguedTag.save()

    def createMenuBar(self):
        self.ui.menubar = QtGui.QMenuBar()
        self.ui.menubar.setGeometry(QtCore.QRect(0, 0, 1756, 21))
        self.ui.menubar.setMinimumSize(QtCore.QSize(0, 0))
        font = QtGui.QFont()
        font.setPointSize(9)
        self.ui.menubar.setFont(font)

        self.ui.menu_Edit = QtGui.QMenu(self.ui.menubar)
        self.ui.menu_Edit.setTitle("&Edit")
        self.ui.menubar.addAction(self.ui.menu_Edit.menuAction())
        self.ui.menu_Tool = QtGui.QMenu(self.ui.menubar)
        self.ui.menu_Tool.setTitle("&Tool")
        self.ui.menubar.addAction(self.ui.menu_Tool.menuAction())
        self.ui.menu_Help = QtGui.QMenu(self.ui.menubar)
        self.ui.menu_Help.setTitle("&Help")
        self.ui.menubar.addAction(self.ui.menu_Help.menuAction())
        self.setMenuBar(self.ui.menubar)

    def createAccountMenuBar(self):
        self.ui.accountMenuBar = QtGui.QMenuBar()
        font = QtGui.QFont()
        font.setPointSize(9)
        self.ui.accountMenuBar.setFont(font)

        self.ui.accountMenu = QtGui.QMenu()
        self.ui.accountMenuBar.addMenu(self.ui.accountMenu)
        self.ui.accountMenu.setTitle("&Username")

        self.ui.accountSettingAction = QtGui.QAction(self.ui.accountMenu)
        self.ui.accountSettingAction.setText("&Account Settings")
        self.ui.accountMenu.addAction(self.ui.accountSettingAction)

        self.ui.signOutAction = QtGui.QAction(self.ui.accountMenu)
        self.ui.signOutAction.setText("&Sign Out")
        self.ui.accountMenu.addAction(self.ui.signOutAction)

        self.ui.menubar.setCornerWidget(self.ui.accountMenuBar)

    def createGroupBox(self,parent=None,title='Group Box',*args):
        groupBox = QtGui.QGroupBox(parent)
        font = QtGui.QFont()
        font.setPointSize(10)
        groupBox.setFont(font)
        groupBox.setTitle(title)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum,QtGui.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(groupBox.sizePolicy().hasHeightForWidth())
        groupBox.setSizePolicy(sizePolicy)
        groupBox.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        groupBox.setFlat(True)

        verticalLayout = QtGui.QVBoxLayout(groupBox)
        treeWidget = TreeWidget(groupBox)
        treeWidget.updateContent()
        verticalLayout.addWidget(treeWidget)

        return (groupBox,treeWidget)

    def createProjectGroupBox(self,parent=None,dir=None,title='Group Box',*args):
        groupBox = QtGui.QGroupBox(parent)
        font = QtGui.QFont()
        font.setPointSize(10)
        groupBox.setFont(font)
        groupBox.setTitle(title)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum,QtGui.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(groupBox.sizePolicy().hasHeightForWidth())
        groupBox.setSizePolicy(sizePolicy)
        groupBox.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        groupBox.setFlat(True)

        verticalLayout = QtGui.QVBoxLayout(groupBox)
        treeWidget = ProjectTreeWidget(parent=groupBox,dir=dir)
        verticalLayout.addWidget(treeWidget)
        return (groupBox,treeWidget)

    def createTypeGroupBox(self,parent=None,dir=None,title='Group Box',*args):
        groupBox = QtGui.QGroupBox(parent)
        font = QtGui.QFont()
        font.setPointSize(10)
        groupBox.setFont(font)
        groupBox.setTitle(title)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum,QtGui.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(groupBox.sizePolicy().hasHeightForWidth())
        groupBox.setSizePolicy(sizePolicy)
        groupBox.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        groupBox.setFlat(True)

        verticalLayout = QtGui.QVBoxLayout(groupBox)
        treeWidget = TypeTreeWidget(parent=groupBox,dir=dir)
        verticalLayout.addWidget(treeWidget)
        return (groupBox,treeWidget)

    def createSideBar(self):
        self.allAssetsWidgets = self.createGroupBox(parent=self.ui.nav_tabWidget,title='ALL ASSETS')
        self.ui.verticalLayout.addWidget(self.allAssetsWidgets[0])

        self.projectWidgets = self.createProjectGroupBox(parent=self.ui.nav_tabWidget,title='ALL PROJECTS',dir=self.projectDir)
        self.ui.verticalLayout_2.addWidget(self.projectWidgets[0])

        self.typeWidgets = self.createTypeGroupBox(parent=self.ui.nav_tabWidget,title='TYPE',dir=self.projectDir)
        self.typeWidgets[1].addItem('cha')
        self.typeWidgets[1].addItem('prp')
        self.typeWidgets[1].addItem('set')
        self.typeWidgets[1].addItem('sence')
        self.typeWidgets[1].addItem('scene')
        self.ui.verticalLayout_2.addWidget(self.typeWidgets[0])

        self.projectWidgets[1].setTypeWidget(typeWidget=self.typeWidgets[1])
        self.typeWidgets[1].setProjectWidget(projectWidget=self.projectWidgets[1])

    def createScrollArea(self):
        self.contentScrollArea = ScrollArea(parent=self.ui.centralwidget,topWidget=self.topWidget)
        self.contentScrollArea.updateContent()
        self.ui.work_verticalLayout.addWidget(self.contentScrollArea)

    def assetDropped(self,urls=None,*args):
        self.contentScrollArea.restoreWidget()

        selectedItem = self.allAssetsWidgets[1].getCurrentSelectedItem()
        if selectedItem:
            self.selectedTag = selectedItem.tag
        else:
            self.selectedTag = Tags.objects.filter(title='uncatelogued')[0]

        if urls:
            for url in urls:
                if os.path.exists(url):
                    # assetUUID = u'' + str(uuid.uuid4().hex)
                    assetName = os.path.basename(url)
                    assetDescription = 'This is a CG asset'
                    assetPublicationDate = datetime.datetime.now().strftime("%Y-%m-%d")
                    # assetMD5 = getFileMd5(url)
                    # assetFile = open(u''+str(url),'rb')
                    f = open(url,'rb')
                    assetFile = File(f)
                    asset = Assets()
                    asset.name = assetName
                    asset.description=assetDescription
                    asset.publication_date=assetPublicationDate
                    asset.asset_file=assetFile
                    asset.save()
                    asset.tags.add(self.selectedTag)
                    assetFile.close()
                    f.close()

            assets = Assets.objects.filter(tags=self.selectedTag)
            self.contentScrollArea.updateContent(assets=assets)

    # def newAssetsBtnCmd(self):
    #     ca_win = CreateAssetDialog(parent=self)
    #     ca_win.show()

    def connections(self):
        self.contentScrollArea.fileDropped.connect(self.assetDropped)
        # self.ui.new_version_pushButton.clicked.connect(self.newAssetsBtnCmd)
        self.allAssetsWidgets[1].filterAssetsSignal.connect(self.contentScrollArea.updateContent)
        self.projectWidgets[1].filterAssetsSignal.connect(self.contentScrollArea.updateContent)
        self.typeWidgets[1].filterAssetsSignal.connect(self.contentScrollArea.updateContent)

    # event : QCloseEvent
    def closeEvent(self,event,*args):
        self.writeSettings()
        event.accept()

    def writeSettings(self):
        self.settings = QtCore.QSettings('CGENTER','AssetEngine')
        self.settings.beginGroup('MainWindow')
        self.settings.setValue('size',self.size())
        self.settings.setValue('pos',self.pos())
        self.settings.endGroup()

    def readSettings(self):
        self.settings = QtCore.QSettings('CGENTER','AssetEngine')
        self.settings.beginGroup('MainWindow')
        size = self.settings.value('size')
        pos = self.settings.value('pos')
        if size:
            self.resize(size)
        if pos:
            self.move(pos)
        self.settings.endGroup()

        self.settings.beginGroup('LoginDialog')
        username = self.settings.value('username')
        if username:
            self.ui.accountMenu.setTitle(username)
        self.settings.endGroup()