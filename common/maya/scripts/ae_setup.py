#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Auther: Godfrey Huang
# Email: huangqiu@cgenter.com
#
# Created by Godfrey Huang on 2015-12-15.
# Copyright (c) 2015 cgenter.com. All rights reserved.
import maya.cmds as cmds
import maya.mel as mel
import re
import sys,os
import inspect

def currentFileDirectory():
	path = os.path.realpath(sys.path[0])
	if os.path.isfile(path):
		path = os.path.dirname(path)
		return os.path.abspath(path)
	else:
		caller_file = inspect.stack()[1][1]
		return os.path.abspath(os.path.dirname(caller_file))

def getParentPath(strPath):
    if not strPath:
        return None
    lsPath = os.path.split(strPath)
    if lsPath[1]:
        return lsPath[0]

    lsPath = os.path.split(lsPath[0])
    return lsPath[0]

def getModuleFolder():
    mayaAppDirTemp = os.getenv("MAYA_APP_DIR")
    modules = 'modules/'
    filePath = mayaAppDirTemp+'/'+modules
    if not os.path.exists(filePath):
        os.makedirs(filePath)
    return filePath

def createModFile():
    modulePath = getParentPath(currentFileDirectory()).replace('\\','/')
    modStr = '+ AssetEngine 1.0 %s\n' % modulePath
    modStr += 'PYTHONPATH +:= ../../'
    modulePath = getModuleFolder()+'/AssetEngine.mod'
    if os.path.exists(modulePath):
        os.remove(modulePath)
    f = open(modulePath,'w')
    f.write(modStr)
    f.close()

def enableaAsetEngine(filename):
    rootPath = getParentPath(currentFileDirectory())
    assetEnginePlugin = rootPath+'/plug-ins/'
    iconPath = rootPath+'/icons'
    assetEngineFilePath = assetEnginePlugin+'assetEngine.py'
    pluginStr = mel.eval('getenv "MAYA_PLUG_IN_PATH";')+';'+assetEnginePlugin
    mel.eval('putenv "MAYA_PLUG_IN_PATH" "%s";' % pluginStr)
    iconStr = mel.eval('getenv "XBMLANGPATH";') + ';' + iconPath
    mel.eval('putenv "XBMLANGPATH" "%s";' % iconStr)
    with open(filename,'a+') as f:
        state = True
        for line in f.readlines():
            if re.findall("assetEngine.py",line):
                state = False
        if state:
            f.write(r'evalDeferred("autoLoadPlugin(\"\", \"assetEngine.py\", \"assetEngine\")");')

    if not cmds.pluginInfo( assetEngineFilePath, query=True, autoload=True):
        cmds.pluginInfo( assetEngineFilePath, edit=True, autoload=True)

    # if not cmds.pluginInfo( GRenderFilePath, query=True, loaded=True ):
    #     cmds.loadPlugin(GRenderFilePath)

def main():
    createModFile()
    pluginsPrefsPath = cmds.internalVar(upd=True)+'pluginPrefs.mel'
    enableaAsetEngine(filename=pluginsPrefsPath)

if __name__=='__main__':
    main()