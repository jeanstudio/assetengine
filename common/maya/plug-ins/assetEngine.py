#!/usr/bin/env python
#-*-coding:utf-8-*-

import sys
import maya.OpenMayaMPx
import maya.OpenMaya as om

kPluginCmdName="assetengine"

class assetengine(maya.OpenMayaMPx.MPxCommand):
    def __init__(self,parent=None,*args):
        maya.OpenMayaMPx.MPxCommand.__init__(self)

    def doIt(self,*args):
        pyString = 'python("import ae_menu as am");'
        pyString += 'python("am.main()");'
        om.MGlobal.executeCommand(pyString)

def creator():
    return maya.OpenMayaMPx.asMPxPtr(assetengine())

def initializePlugin(mObject):
    mPlugin=maya.OpenMayaMPx.MFnPlugin(mObject)
    try:
        mPlugin.registerCommand(kPluginCmdName, creator)
        pyString = 'python("import ae_menu as am");'
        pyString += 'python("am.main()");'
        om.MGlobal.executeCommand(pyString)
    except:
        sys.stderr.write('Faild load plugin')
        raise

def uninitializePlugin(mObject):
    mPlugin=maya.OpenMayaMPx.MFnPlugin(mObject)
    try:
        mPlugin.deregisterCommand(kPluginCmdName)
        commandString=""
        commandString += "deleteUI \"assetEngineMenu\";\n"
        om.MGlobal.executeCommand(commandString)
    except:
        sys.stderr.write('Failed Unload plugin')
        raise
